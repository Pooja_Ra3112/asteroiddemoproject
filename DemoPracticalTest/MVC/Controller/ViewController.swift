//
//  ViewController.swift
//  DemoPracticalTest
//
//  Created by Pooja Raghuwanshi on 21/07/21.
//

import UIKit
import Charts
import Toast

class ViewController: UIViewController {
    @IBOutlet weak var chtChart: LineChartView!

    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var viewLoader: UIView!
    @IBOutlet weak var txtEndDate: UITextField!
    @IBOutlet weak var txtStartDate: UITextField!
    var Apikey = "DUjY4hYL6tzYRa2lwSfSHSoHBO6Gl9WjZB716XNT"

    //    let months = ["Jan", "Feb", "Mar","Apr"]
    //    var numbers : [Double] = [1,10,20,25,30]
    var months = [String]()
    var numbers = [Double]()

    override func viewDidLoad() {
        super.viewDidLoad()
        let S_Date = currentDate()
        self.DataApiCall(start_date: S_Date, End_Date: S_Date)
    }
    //MARK:- Line Chart Draw

    func updateGraph(){
        var lineChartEntry  = [ChartDataEntry]() //this is the Array that will eventually be displayed on the graph.
        //here is the for loop
        for i in 0..<numbers.count {

            let value = ChartDataEntry(x: Double(i), y: numbers[i]) // here we set the X and Y status in a data chart entry

            lineChartEntry.append(value) // here we add it to the data set
        }

        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Asteroid") //Here we convert lineChartEntry to a LineChartDataSet

        line1.colors = [NSUIColor.systemBlue] //Sets the colour to blue


        let data = LineChartData() //This is the object that will be added to the chart

        data.addDataSet(line1) //Adds the line to the dataSet
        
        chtChart.xAxis.valueFormatter = IndexAxisValueFormatter(values:months)
        chtChart.xAxis.granularity = 1
        chtChart.xAxis.labelPosition = .top

        // Set the x values date formatter
       
        
        chtChart.data = data //finally - it adds the chart data to the chart and causes an update

        chtChart.chartDescription?.text = "" // Here we set the description for the graph

    }
    
    //MARK:- ConvertFormate

    func currentDate()-> String
    {
        let date = Date()
        let current_time = date.dateString("YYYY-MM-dd")
        return current_time

    }

    func changeDate(sDate:String)->Date
    {
        // Create Date Formatter
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let date = dateFormatter.date(from: sDate)
        return date!
    }
    
    func ConvertDate(sDate:String)->String
    {
        // Create Date Formatter
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let date = dateFormatter.date(from: sDate)
        let current_time = date!.dateString("dd")

        return current_time
    }
  


    //MARK:- Api
    func DataApiCall(start_date : String, End_Date : String)
    {
        self.viewLoader.isHidden = false
        let urlString = "https://api.nasa.gov/neo/rest/v1/feed?start_date=\(start_date)&api_key=\(self.Apikey)&end_date=\(End_Date)"
        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        URLSession.shared.dataTask(with: url!) { (data, res, err) in
            guard let data = data else {
                print("Data != Data")
                self.view.makeToast("Date Format Exception - Expected format (yyyy-mm-dd) - The Feed date limit is only 7 Days")
                return
            }
            do {
                if let httpStatus = res as? HTTPURLResponse, httpStatus.statusCode == 400 {
                   // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    DispatchQueue.global(qos: .background).async {
                        DispatchQueue.main.async {
                            self.viewLoader.isHidden = true

                            self.view.makeToast("Date Format Exception - Expected format (yyyy-mm-dd) - The Feed date limit is only 7 Days")
                        }
                    }

                }
                else
                {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    self.numbers.removeAll()
                    self.months.removeAll()
                    print(json)
                    let dic = json as! NSDictionary


                    if let near_earth_objects =  dic["near_earth_objects"] as? NSDictionary {
                        for result in near_earth_objects  {
                           
                            if let Date_get =  result.value as? NSArray {
                                let close_approch_data = Date_get.value(forKey: "close_approach_data") as? NSArray
                                if let miss_distance = close_approch_data!.value(forKey: "miss_distance")as? NSArray
                                {
                                    let Asteroid_Dist = (miss_distance.value(forKey: "lunar") as! NSArray).mutableCopy() as! NSMutableArray
                                    for miles_distance in Asteroid_Dist  {
                                        let Distatnce = miles_distance as! NSArray
                                        let dist_data = Distatnce[0] as! NSString

                                        self.numbers.append(Double(dist_data as Substring)!)
                                    }

                                }
                                if start_date == End_Date
                                {
                                    self.months.append(start_date)
                                }
                                else
                                {
                                    if let close_approach_date = close_approch_data!.value(forKey: "close_approach_date")as? NSArray
                                    {
                                        for date in close_approach_date  {
                                            let req_date = date as! NSArray
                                            let date_data = req_date[0] as! NSString
                                            let exact_date = self.ConvertDate(sDate: date_data as String)
                                            self.months.append(exact_date as String)
                                        }
                                        
                                    }

                                }


                                
                            }
                        }
                    }
                    
                    if self.numbers.count != 0 && self.months.count != 0
                    {
                        self.months.sort()
                        DispatchQueue.global(qos: .background).async {
                            DispatchQueue.main.async {
                                self.viewLoader.isHidden = true
                                self.updateGraph()
                            }
                        }
                    }
                    
                }

            } catch {
                print("didnt work")
                self.view.makeToast("didnt work")
            }
        }.resume()
    }
    


    //MARK:- Button Action

    @IBAction func btnStartDate(_ sender: UIButton) {
        let sDate = Date()
        RPicker.selectDate(title: "Select Start Date", cancelText: "Cancel", doneText: "Done", datePickerMode: .date, selectedDate: sDate, minDate: sDate, maxDate: nil, style: .Wheel) { (selectedDate) in
            // TODO: Your implementation for date
            self.txtStartDate.text = selectedDate.dateString("YYYY-MM-dd")
            self.txtStartDate.resignFirstResponder()
            self.view.frame.origin.y = 0
        }

    }
    
    @IBAction func btnEndDate(_ sender: UIButton) {
        if txtStartDate.text != ""
        {
            let startDate = self.changeDate(sDate: self.txtStartDate.text!)
            let sDate = Date()
            RPicker.selectDate(title: "Select End Date", cancelText: "Cancel", doneText: "Done", datePickerMode: .date, selectedDate: startDate, minDate: sDate, maxDate: nil, style: .Wheel) { (selectedDate) in
                // TODO: Your implementation for date
                self.txtEndDate.text = selectedDate.dateString("YYYY-MM-dd")
                self.txtEndDate.resignFirstResponder()
                self.view.frame.origin.y = 0
                self.DataApiCall(start_date: self.txtStartDate.text!, End_Date: self.txtEndDate.text!)
            }
        }
        else
        {
            
            self.view.makeToast("Please select start date first")
        }
    }
}

